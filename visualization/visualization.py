import cv2
import numpy as np

from visualization.show_window import ShowWindow
from visualization.plot import draw_plot


class Visualization(ShowWindow):
    def __init__(self, states_real, states_estimated, measurements, time, width=1200, height=600):
        self.delta_time = time[1] - time[0]
        self.delay_time = int(self.delta_time * 1000)

        super().__init__('Inverted Pendulum', (width, height), self.delay_time)

        self.states_real = states_real
        self.states_estimated = states_estimated
        self.measurements = measurements
        self.time = time
        self.canvas = np.ones((height, width))
        self.width = width
        self.height = height

    def show_simulation(self):
        self.init_window()
        while not self.exit:
            for i, (_, theta, _, position) in enumerate(self.states_real):
                status, image = self._draw(self.canvas, position, theta,
                                           self.measurements[i],
                                           self.delta_time * i)
                if self.exit or not self.is_window_open():
                    self.exit = True
                    break
                elif not status:
                    break

                self.update_window(image)

        self.destroy_window()

    def _draw(self, image, position, theta, theta_measured, time):
        image = np.array(image)
        # text
        cv2.putText(image,
                    'Position: {:+06.3f} m   Theta: {:+06.2f} deg   Time: {:06.2f} s'.format(
                        position, theta * 180 / np.pi, time), (210, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, .8, 0, 2)

        # line
        y = int(self.height * 0.75)
        cv2.line(image, (0, y), (self.width, y), 0.5, 3)

        # box
        x_scale = self.width * .5
        dy = 20
        dx = 30
        x = int(x_scale * position + self.width / 2)
        if not 0 < x < self.width:
            return False, image
        cv2.rectangle(image, (x - dx, y - dy), (x + dx, y + dy), 0, cv2.FILLED)

        # pendulum shadow
        l = 2 * y / 3
        dx = int(l * np.sin(theta_measured))
        dy = int(l * np.cos(theta_measured))
        if dy < 0:
            return False, image
        cv2.line(image, (x, y), (x + dx, y - dy), 0.2, 8)

        # pendulum
        dx = int(l * np.sin(theta))
        dy = int(l * np.cos(theta))
        if dy < 0:
            return False, image
        cv2.line(image, (x, y), (x + dx, y - dy), 0, 8)

        return True, image

    def show_plot(self):
        draw_plot(self.name, self.states_real, self.states_estimated, self.measurements, self.time)
