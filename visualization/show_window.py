import cv2


class ShowWindow:
    def __init__(self, name, window_size=(800, 600), delay_time=1000):
        self.name = name
        self.window_size = window_size
        self.exit = False
        self._time = delay_time

    def init_window(self):
        cv2.namedWindow(self.name, cv2.WINDOW_GUI_NORMAL)
        cv2.resizeWindow(self.name, *self.window_size)

    def destroy_window(self):
        cv2.destroyWindow(self.name)

    def key_events(self, key):
        """Override this method to servicing key events"""
        if key == 27:
            self.exit = True

    def is_window_open(self):
        return (cv2.getWindowProperty(self.name, cv2.WND_PROP_VISIBLE) and
                cv2.getWindowProperty(self.name, cv2.WND_PROP_FULLSCREEN) in
                (cv2.WINDOW_NORMAL, cv2.WINDOW_FULLSCREEN))

    def update_window(self, frame):
        cv2.imshow(self.name, frame)
        key = cv2.waitKeyEx(self._time)
        self.key_events(key)

