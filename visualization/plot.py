import matplotlib.pylab as plt
import numpy as np


def draw_plot(title, states_real, states_estimated, measurements, time):
    fig = plt.figure(title)

    ax = fig.add_subplot(311)
    ax.plot(time, measurements * 180 / np.pi, 'r.', label='measurement')
    ax.plot(time, states_real[:, 1] * 180 / np.pi, 'g', label='real')
    ax.plot(time, states_estimated[:, 1] * 180 / np.pi, 'orange', label='estimated')
    ax.axhline(0, color='k')
    ax.set_xlabel('time [s]')
    ax.set_ylabel('theta [deg]')
    ax.legend()

    bx = fig.add_subplot(312)
    bx.plot(time, states_real[:, 2], label='real')
    bx.axhline(0, color='k')
    bx.set_xlabel('time [s]')
    bx.set_ylabel('velocity [m/s]')
    bx.legend()

    cx = fig.add_subplot(313)
    cx.plot(time, states_real[:, 3], label='real')
    cx.set_xlabel('time [s]')
    cx.set_ylabel('position [m]')
    cx.legend()

    plt.show()
