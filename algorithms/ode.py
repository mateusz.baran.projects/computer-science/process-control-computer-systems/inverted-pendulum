import numpy as np


def rk4(A, X, B, U, dt):
    k1 = np.dot(A, X) + np.dot(B, U)
    k2 = np.dot(A, X + dt / 2. * k1) + np.dot(B, U)
    k3 = np.dot(A, X + dt / 2. * k2) + np.dot(B, U)
    k4 = np.dot(A, X + dt * k3) + np.dot(B, U)
    return dt / 6. * (k1 + 2. * k2 + 2. * k3 + k4)
