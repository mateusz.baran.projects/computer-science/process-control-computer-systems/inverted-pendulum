import numpy as np


class PID:
    def __init__(
            self, kp=0., ki=0., kd=0., *_,
            dt=1., target=0.,
            u_min=-np.inf, u_max=np.inf,
            integral_error_min=-np.inf, integral_error_max=np.inf,
            loss_function=None
    ):
        self.kp = kp
        self.ki = ki
        self.kd = kd

        assert dt > 0., 'dt must be greater than zero'
        self.dt = dt

        self.u_min = u_min
        self.u_max = u_max

        self.integral_error_min = integral_error_min
        self.integral_error_max = integral_error_max

        self.previous_error = 0.
        self.integral_error = 0.

        self.loss = 0.
        self.loss_function = loss_function

        self.target = target
        self.error = 0.
        self.u_value = 0.

    def __call__(self, current_value):
        self.error = self.target - current_value

        p_value = self.kp * self.error

        self.integral_error += self.error * self.ki * self.dt
        self.integral_error = np.clip(self.integral_error, self.integral_error_min, self.integral_error_max)
        i_value = self.integral_error

        d_value = self.kd * (self.error - self.previous_error) / self.dt
        self.previous_error = self.error

        self.u_value = p_value + i_value + d_value

        if self.loss_function:
            self.loss += self.loss_function()

        return np.clip(self.u_value, self.u_min, self.u_max)
