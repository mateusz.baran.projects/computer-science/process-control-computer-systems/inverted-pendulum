import numpy as np

from algorithms.ode import rk4


class KalmanFilterSimulator:
    def __init__(self, X, P, A, Q, B, U, Y, H, R, time, u_func, lambda_=1):
        self.X = X
        self.X_est = np.array(X)
        self.P = P
        self.A = A
        self.Q = Q
        self.B = B
        self.U = U

        self.Y = Y
        self.Y_est = np.array(Y)
        self.H = H
        self.R = R

        self.time = time
        self.u_func = u_func
        self.lambda_ = lambda_

        self.estimated_error = np.empty_like(Y)
        self.measured_error = np.empty_like(Y)

    def run(self, filter_enable=True):
        self.estimated_error *= 0
        self.measured_error *= 0

        dt = self.time[1] - self.time[0]
        nt = len(self.time)

        X_real = np.empty([nt, *self.X.shape])
        X_estimated = np.empty([nt, *self.X.shape])
        Y_measurements = np.empty([nt, *self.Y.shape])

        X_real[0] = self.X
        X_estimated[0] = self.X
        Y_measurements[0] = self.Y

        for i in range(nt - 1):
            # Control
            if filter_enable:
                self.U = self.u_func(np.sum(self.Y_est))
            else:
                self.U = self.u_func(np.sum(self.Y))

            # Simulation
            Y = np.dot(self.H, self.X)
            self.Y = Y + np.random.normal(0, self.R)
            X = rk4(self.A, self.X, self.B, self.U, dt)
            self.X = self.X + X + np.random.normal(0, self.Q)

            if filter_enable:
                # Prediction
                self.X_est = self.X_est + rk4(self.A, self.X_est, self.B, self.U, dt)
                self.P = np.dot(self.A, np.dot(self.P, self.A.T)) + self.Q

                # Update
                IS = self.R + np.dot(self.H, np.dot(self.P, self.H.T))
                K = np.dot(self.P, np.dot(self.H.T, np.linalg.inv(IS)))
                self.Y_est = np.dot(self.H, self.X_est)
                self.X_est = self.X_est + self.lambda_ * np.dot(K, (self.Y - self.Y_est))
                self.P = self.P - np.dot(K, np.dot(IS, K.T))
                self.Y_est = np.dot(self.H, self.X_est)

            # Save
            self.measured_error += np.abs(Y - self.Y)

            X_real[i + 1] = self.X
            Y_measurements[i + 1] = self.Y

            if filter_enable:
                self.estimated_error += np.abs(Y - self.Y_est)
                X_estimated[i + 1] = self.X_est

        return (np.squeeze(X_real, axis=2),
                np.squeeze(X_estimated, axis=2),
                np.squeeze(Y_measurements, axis=2))
