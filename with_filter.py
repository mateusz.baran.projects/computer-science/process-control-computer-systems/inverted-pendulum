from config import KFS, t
from visualization.visualization import Visualization


def main():
    states_real, states_estimated, measurements = KFS.run()

    print('Estimated error: {:.3f}'.format(KFS.estimated_error[0][0]))
    print('Measured error:  {:.3f}'.format(KFS.measured_error[0][0]))
    print('Performance:     {:.2%}'.format(1 - (KFS.estimated_error/KFS.measured_error)[0][0]))

    visualization = Visualization(states_real, states_estimated, measurements[:, 0], t)
    visualization.show_plot()
    visualization.show_simulation()


if __name__ == '__main__':
    main()
