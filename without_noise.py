from config import KFS, t
from visualization.visualization import Visualization


def main():
    # remove noise
    KFS.R *= 0
    KFS.Q *= 0

    states_real, states_estimated, measurements = KFS.run(filter_enable=False)

    visualization = Visualization(states_real, states_estimated * 0, measurements[:, 0], t)
    visualization.show_plot()
    visualization.show_simulation()


if __name__ == '__main__':
    main()
