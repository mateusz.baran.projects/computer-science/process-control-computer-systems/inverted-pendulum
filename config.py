import numpy as np

from algorithms.controller import PID
from algorithms.kalman_filter import KalmanFilterSimulator
from baselines.model import inverted_pendulum


h = 1e-2
T = 6.
t = np.arange(0., T, h)

X, A, P, U, B, Q, H, Y, R = inverted_pendulum(
    theta0=.1,
    M=.5,
    m=.2,
    L=.3,
    b=.1,
    g=9.81,
    sigma_theta=np.pi / 30,
    sigma_velocity=1e-2,
)

pid = PID(
    -10.55263852, -18.75075395, -2.430921,
    dt=h,
    target=0,
    u_min=-10, u_max=10,
)

KFS = KalmanFilterSimulator(X, P, A, Q, B, U, Y, H, R, t, pid, lambda_=.3)


