from config import KFS, t
from visualization.visualization import Visualization


def main():
    states_real, states_estimated, measurements = KFS.run(filter_enable=False)

    print('Measured error:  {:.3f}'.format(KFS.measured_error[0][0]))

    visualization = Visualization(states_real, states_estimated * 0, measurements[:, 0], t)
    visualization.show_plot()
    visualization.show_simulation()


if __name__ == '__main__':
    main()
