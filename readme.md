# MODELOWANIE I SYMULOWANIE ZACHOWANIA ODWRÓCONEGO WAHADŁA

## Streszczenie
Opisano   problematykę   sterowania   odwróconego   wahadła
regulatorem   PID   z   uwzględnieniem   niepewności   pomiarowej   kąta
przechylenia   oraz   prędkości   liniowej   wózka.   Rozwiązanie   przedstawia
wykorzystanie algorytmów optymalizacji do dobrania nastaw regulatora PID
oraz użycie filtru Kalmana do zniwelowania niepewności odczytów z sensorów.
Wyniki badań przedstawiono na wykresach.

[Czytaj więcej...](https://gitlab.com/neckyGP/computer-science/process-control-computer-systems/inverted-pendulum/blob/master/article.pdf)

## Instalacja
```bash
git clone https://gitlab.com/neckyGP/computer-science/process-control-computer-systems/inverted-pendulum.git inverted_pendulum
cd inverted_pendulum
pip install -r requirements.txt
```

## Konfiguracja
Aby dokonać zmiany parametrów modelu lub symulacji należy edytować plik config.py
```bash
nano config.py
```

## Uruchomienie symulacji
* stabilizacja modelu matematycznego
```bash
python without_noise.py
```

* sterowanie z zaszumieniem pomiarów
```bash
python without_filter.py
```

* odszumianie filtrem Kalmana
```bash
python with_filter.py
```

## Wyniki
Wyniki przeprowadzonego doświadczenia świadczą o poprawie sterowania w granicach 55%, 
co jest satysfakcjonującym wynikiem ze względu na to, że odwrócone 
wahadło jest bardzo niestabilnym układem.