import numpy as np
from scipy.optimize import minimize
from tqdm import trange

from algorithms.controller import PID
from algorithms.kalman_filter import KalmanFilterSimulator
from baselines.model import inverted_pendulum


def f(p):
    h = 1e-2
    T = 3.
    t = np.arange(0., T, h)

    X, A, P, U, B, Q, H, Y, R = inverted_pendulum(
        theta0=.1,
        M=.5,
        m=.2,
        L=.3,
        b=.1,
        g=9.81,
        sigma_theta=np.pi / 180,
        sigma_velocity=1e-3,
    )

    pid = PID(
        p[0], p[1], p[2],
        dt=h,
        target=0,
        u_min=-10, u_max=10,
        loss_function=lambda: (pid.error * pid.dt) ** 2,
    )

    # remove noise
    R *= 0
    Q *= 0

    KalmanFilterSimulator(X, P, A, Q, B, U, Y, H, R, t, pid).run(filter_enable=False)

    return pid.loss


def selection(n=1000):
    min_fun = np.inf
    best_p = None
    for _ in trange(n):
        p = minimize(f, (np.random.random(3) - 1) * 50)
        if p.fun < min_fun:
            min_fun = p.fun
            best_p = p

    return best_p


if __name__ == '__main__':
    params = selection(n=10)
    print(params)

    # best
    # x: array([-10.55263852, -18.75075395, -2.430921]) fun: 2.0827784651577442e-05
