import numpy as np


def inverted_pendulum(theta0=.1, M=.5, m=.2, L=.3, b=.1, g=9.81, sigma_theta=np.pi / 180,
                      sigma_velocity=1e-3):
    # Initialization of state matrices   X = [theta', theta, x', x]
    X = np.array([[0.0], [theta0], [0.0], [0.0]])
    A = np.array([
        [0, (M + m) * g / (M * L), b / (M * L), 0],
        [1, 0, 0, 0],
        [0, -m * g / M, -b / M, 0],
        [0, 0, 1, 0],
    ])
    P = np.eye(X.shape[0])
    U = np.array([[0.]])
    B = np.array([
        [-1 / (M * L)],
        [0.],
        [1 / M],
        [0.],
    ])
    Q = np.array([[0], [0], [0], [0]])

    # Measurement matrices
    H = np.array([[0., 1., 0., 0.],
                  [0., 0., .1, 0.]])

    Y = H @ X
    R = np.array([
        [sigma_theta],
        [sigma_velocity]
    ]) ** 2
    return X, A, P, U, B, Q, H, Y, R
